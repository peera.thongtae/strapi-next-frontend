import type { NextPage } from 'next'
import Head from 'next/head';
import React from 'react'
import useSWR from 'swr';
import Errors from '../../components/Errors';
import Loading from '../../components/Loading';
import ShopCard from '../../components/Shop/ShopCard';
import { fetcher } from '../../utils/fetcher';

const ShopPage: NextPage = () => {
  const { data: shops, error } = useSWR(
    {
      url: `/shops`,
      options: {
        populate: 'image,shop_categories'
      },
    },
    fetcher
  );

  if(!shops) return <Loading />
  if (error) return <Errors message={error.message} status={error.status} error={error} />;

  console.log(shops);

  return (
    <>
      <Head>
        <title>Shops</title>
      </Head>
      <div className="w-full mt-4 px-24 ">
        <span className="flex justify-center font-bold">Shop Lists</span>
        <div className="grid grid-cols-4 gap-3 mt-24">
          {shops.data.map((shop: any, index: number) => {
            return <ShopCard shop={shop} />
          })}
        </div>
      </div>
    </>
   
    
  )
};


export default ShopPage
