import '../styles/globals.css'
import '../styles/card.scss'
import type { AppProps } from 'next/app'
import Layout from '../components/Layout'
import '@fortawesome/fontawesome-svg-core/styles.css';
import { config } from '@fortawesome/fontawesome-svg-core';
import React from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import NProgress from 'nprogress';
import { Router } from 'next/router';
import Head from 'next/head';
import '../styles/nprogress.css';
import { SWRConfig } from 'swr';
import FilterContextProvider from '../lib/context/FilterContext';

config.autoAddCss = false;

// when change url
Router.events.on('routeChangeStart', (url) => {
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <SWRConfig>
          <FilterContextProvider>
            <Layout>
              <ToastContainer />
              <Component {...pageProps} />
            </Layout>
          </FilterContextProvider>
      </SWRConfig>
    </>
  );
}

export default MyApp
