import axios from "axios";
import { API_URL } from "../config";

const Options = (options: any) => {
  if (typeof window === "undefined") return {};

  if (!window.localStorage.user) return {};

  if (Object.keys(window.localStorage.user).length === 0) return {};

  const token = window.localStorage.token;
  console.log(token);
  if (!!token) {
    return {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      params: options,
    };
  }
}

type FetcherProps = {
  url: string,
  options?: Object,
};

export const fetcher = async (fetcherProps: FetcherProps) => {
  const { url, options } = fetcherProps;
  const { data } = await axios.get(`${API_URL}${url}`, Options(options));

  return data;
}
