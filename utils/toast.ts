import { toast } from 'react-toastify';

// const ToastHelper = (id: any, payload: any) => {
//   const { response, title, isLoading, autoClose } = payload;
//   return toast.update(id, {
//     render: title || 'Toast',
//     type: response.success ? 'success' : 'error',
//     autoClose: autoClose || 2000,
//     isLoading: isLoading || false,
//   });
// };


const ToastHelper = {
  update: (id: any, payload: any) => {
      const { response, title, isLoading, autoClose } = payload;
      return toast.update(id, {
        render: title || 'Toast',
        type: response.success ? 'success' : 'error',
        autoClose: autoClose || 2000,
        isLoading: isLoading || false,
      });
  },

  error: (payload: any) => {
    const { title } = payload;
    return toast.error(title, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  },

  success: (payload: any) => {
    const { title } = payload;
    return toast.success(title, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  }
}

export default ToastHelper;