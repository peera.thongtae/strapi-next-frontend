export interface SignUp {
  email: String;
  password: String;
  confirmPassword: String;
  username: String;
  firstName: String;
  lastName: String;
}

export interface EditProfile {
  email: string;
  username: string;
  firstName: string;
  lastName: string;
}