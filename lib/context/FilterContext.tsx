import Router from 'next/router';
import React, { useState } from 'react';

const defaultState = {
  filter: {},
};

const FilterContext = React.createContext<any>(defaultState);

const FilterContextProvider = ({children}: any) => {
  const [filter, setFilter] = useState({});

  const setQueryFilter = (req: any) => {
    setFilter({ ...filter, ...req });
  }
  const value = { filter, setFilter, setQueryFilter };
  const handleClick = () => {
    Router.push('/');
  }

  return (
    <FilterContext.Provider value={value}>
      {children}
      {/* <button
        type="submit"
        className="w-full py-3 bg-gray-800 rounded-full
                  font-medium text-white uppercase
                  focus:outline-none hover:bg-green-700 hover:shadow-none"
        onClick={handleClick}
      >
        Search
      </button> */}
    </FilterContext.Provider>
  )
}

export const useFilter = () => {
  const state = React.useContext(FilterContext);
  return state;
};

export default FilterContextProvider;