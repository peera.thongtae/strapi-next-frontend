import axios from 'axios';
import { API_URL } from '../../config';

const AuthService = {
  async login(email: string, password: string) {
    try {
      const response = await axios.post(`${API_URL}/auth/local`, {
        identifier: email,
        password,
      });
      return response;
    } catch (err) {
      return err.response;
    }
  },
  async register(data: any) {
    try {
      console.log(data);
      const response = await axios.post(`${API_URL}/auth/local/register`, data);
      return response;
    } catch (err) {
      console.log(err.response);
      return err.response;
    }
  },
  getProfile() {
    return axios.get(`${API_URL}/users/me`, {
      headers: this.authHeader(),
    });
  },
  logout() {
    localStorage.removeItem('token');
  },
  getToken() {
    return localStorage.getItem('token');
  },
  saveToken(token: string) {
    localStorage.setItem('token', token);
  },
  authHeader() {
    return { Authorization: `Bearer ${this.getToken()}` };
  },
};

export default AuthService;
