import { useEffect, useState } from "react";
import { fetchShopCategories } from "../api/shop-categories";

const useFetchShopCategories = () => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    fetchShopCategories().then((res) => {
      setCategories(res.data.data);
    }).catch((err) => {
      console.log(err);
    })
  }, []);

  return {
    categories,
  }
};

export default useFetchShopCategories;