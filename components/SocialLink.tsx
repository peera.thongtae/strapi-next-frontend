import {
  faFacebook,
  faImdb,
  faInstagram,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import { faQuestion } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

const linkExternal = (type: string, external: string) => {
  switch (type) {
    case 'facebook':
      return {
        icon: faFacebook,
        link: `https://facebook.com/${external}`,
      };
    case 'instagram':
      return {
        icon: faInstagram,
        link: `https://instagram.com/${external}`,
      };
    case 'twitter':
      return {
        icon: faTwitter,
        link: `https://twitter.com/${external}`,
      };
    case 'imdb':
      return {
        icon: faImdb,
        link: `https://imdb.com/title/${external}`,
      };
    default:
      return {
        icon: faQuestion,
        link: `/`,
      };
  }
};
const SocialLink = ({ external, type }: any) => {
  const linkDetail = linkExternal(type, external);
  return external ? (
      
    <div className="text-gray-500 ml-2 hover:text-red-600 cursor-pointer">
      <a target="_blank" href={linkDetail.link} >
        <FontAwesomeIcon icon={linkDetail.icon} size="lg" />
      </a>
    </div>
  ) : (
    <div className="text-gray-500 cursor-not-allowed ml-2">
      <FontAwesomeIcon icon={linkDetail.icon} size="lg" />
    </div>
  );
};

export default SocialLink;
