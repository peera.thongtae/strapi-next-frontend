import Link from 'next/link';
import React from 'react';
import useSWR, { mutate } from 'swr';
import checkLogin from '../utils/checkLogin';
import { fetcher } from '../utils/fetcher';
import storage from '../utils/storage';
import DropdownProfile from './DropdownProfile';
import NavLeftMenu from './NavLeftMenu';

const Header = () => {
  const { data: user } = useSWR('user', storage);
  const isLoggedIn = checkLogin(user);

  const { data: res, error: errorUser } = useSWR(
    {
      url: `/users/me`,
      options: {},
    },
    fetcher
  );

  if (res) {
    window.localStorage.setItem('user', JSON.stringify(res));
  }
  return (
    <header className="absolute top-0 left-0 right-0 z-20 ">
      <nav className="shadow-lg">
        <div className="container flex flex-col items-center justify-between px-4 py-6 mx-auto md:flex-row">
          
          <Link href="/">
            <span className="flex items-center space-x-2 cursor-pointer">
              <span className="text-2xl font-bold tracking-wide uppercase">
                STRAPI SHOP
              </span>
            </span>
          </Link>
          <NavLeftMenu />
          <div className="relative z-50 mt-3 md:mt-0">
            <ul className="flex items-center flex-1 space-x-4 text-sm">
              {isLoggedIn ? (
                <li className="mt-3 md:ml-6 md:mt-0">
                  <DropdownProfile profile={user} />
                </li>
              ) : (
                <>
                  <li className="mt-3 md:ml-6 md:mt-0">
                    <Link href="signup">
                      <span className="text-gray-800 text-sm font-semibold px-4 py-1 hover:text-purple-600 hover:border-purple-600 link-to">
                        Sign up
                      </span>
                    </Link>
                  </li>
                  <li className="mt-3 md:ml-6 md:mt-0">
                    <Link href="/login">
                      <span className="text-gray-800 text-sm font-semibold border px-4 py-1 rounded-lg hover:text-purple-600 hover:border-purple-600 link-to">
                        Sign in
                      </span>
                    </Link>
                  </li>
                </>
              )}
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
