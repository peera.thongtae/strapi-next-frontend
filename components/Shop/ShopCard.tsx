import React from 'react';
import Image from '../Image';
import Tag from '../Tag';

const ShopCard = (props: any) => {
  const { shop } = props;
  const { attributes } = shop;
  const { shop_categories } = attributes;
  return (
    <div className="max-w-xs rounded-md overflow-hidden shadow-lg hover:scale-105 transition duration-500 cursor-pointer">
      <div>
        <Image media={attributes?.image?.data?.attributes} />
      </div>
      <div className="py-4 px-4 bg-white">
        <h3 className="text-lg font-semibold text-gray-600">{attributes.name}</h3>
        <p className="mt-4 text-md font-thin">{attributes.description}</p>
        <div className="mt-4 text-md font-thin">
          category :
          {shop_categories && shop_categories.data.map((category: any) => {
            return <Tag title={category.attributes.name} to={`/category/${category.id}`} />;
          })}
        </div>
      </div>
    </div>
  );
}

export default ShopCard;