import Link from 'next/link';
import React from 'react';

const Tag = (props: any) => {
  const { to, title } = props;
  return (
    <Link href={to}>
      <span
        className="inline-block rounded-full text-white 
                  bg-black hover:bg-gray-500 duration-300 
                  text-xs font-bold 
                  mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 
                  opacity-90 hover:opacity-100 cursor-pointer"
      >
        {title}
      </span>
    </Link>
  );
};

export default Tag;
