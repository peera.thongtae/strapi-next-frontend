import { getStrapiMedia } from "../utils/medias"
import NextImage, { ImageLoader, ImageLoaderProps } from "next/image"
import { useRouter } from "next/router"

const Image = (props: any) => {
  console.log(props);
  if (props.media === undefined) {
    return <NextImage {...props} />
  }
  const { url, alternativeText } = props.media;
  const loader = ({ src }: any): any => {
    console.log(src);
    if (!src) return 'https://user-images.githubusercontent.com/582516/98960633-6c6a1600-24e3-11eb-89f1-045f55a1e494.png';
    return getStrapiMedia(src)
  }

  return (
    <NextImage
      loader={loader}
      layout="responsive"
      objectFit="cover"
      width={props.media.width}
      height={props.media.height}
      src={url}
      alt={alternativeText || ""}
    />
  )
}

export default Image
