import Link from 'next/link';
import React from 'react';
import useSWR from 'swr';
import checkLogin from '../utils/checkLogin';
import storage from '../utils/storage';

const NavLeftMenu = (props: any) => {
  const { data: user } = useSWR('user', storage);
  const isLoggedIn = checkLogin(user);
  return (
    <ul className="flex items-center flex-1 space-x-4 text-sm">
      {isLoggedIn && <li className="mt-3 md:ml-16 md:mt-0">
        <Link href="/shop/create" as="/shop/create" passHref>
          <span
            className={`tracking-wide uppercase link-to
                    font-bold hover:text-orange-500
                    
                `}
          >
            Create shop
          </span>
        </Link>
      </li>}
      <li className="mt-3 md:ml-16 md:mt-0">
        <Link href="/shop" as="/shop" passHref>
          <span
            className={`tracking-wide uppercase link-to
                    font-bold hover:text-orange-500
                    
                `}
          >
            Shops
          </span>
        </Link>
      </li>
      <li className="mt-3 md:ml-16 md:mt-0">
        <Link href="/category">
          <span
            className={`tracking-wide uppercase link-to
                    font-bold hover:text-orange-500
                `}
          >
            Categories
          </span>
        </Link>
      </li>
    </ul>
  );
};

export default NavLeftMenu;
