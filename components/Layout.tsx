import React from "react";
import Header from "./Header";

const Layout = ({ children, loader, modal }: any) => {
  return (
    <>
      <main className="md:pt-20 pb-20">
        <Header />
        <div className="h-full pt-12">
          {children}
        </div>
      </main>

    </>
  );
};

export default Layout;