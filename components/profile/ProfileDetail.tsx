import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { mutate } from 'swr';
import AuthService from '../../lib/api/auth';
import { EditProfile } from '../../lib/types/auth';
import ToastHelper from '../../utils/toast';
import InputCustom from '../InputCustom';

interface ProfileDetailInterface {
  user: any;
  edit?: boolean;
}

const ProfileDetail = (props: ProfileDetailInterface) => {
  const { user, edit } = props;
  return (
    <div className="w-full relative mt-4 my-24 overflow-hidden">
      {/* <div className="top h-24 w-full overflow-hidden relative" >
        <div className="flex flex-col justify-center items-center relative h-full">
          <h1 className="text-2xl font-semibold">Antonia Howell</h1>
        </div>
      </div> */}
      <div className="grid grid-cols-12 p-12">

        <div className="col-span-12 w-full px-3 py-6 justify-center flex space-x-4  md:space-x-0 md:space-y-4 md:flex-col md:col-span-2 md:justify-start ">
          <a href="#" className="text-sm p-2 bg-indigo-900 text-white text-center rounded font-bold">Basic Information</a>
          <a href="#" className="text-sm p-2 bg-indigo-200 text-center rounded font-semibold hover:bg-indigo-700 hover:text-gray-200">Another Information</a>
          <a href="#" className="text-sm p-2 bg-indigo-200 text-center rounded font-semibold hover:bg-indigo-700 hover:text-gray-200">Another Something</a>
        </div>
        <BasicInformation user={user} edit={edit} />
        
      </div>
    </div>
  );
};

const BasicInformation = (props: any) => {
  const { user, edit } = props;
  const [loading, setLoading] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const submitEditProfile = async (req: EditProfile) => {
    setLoading(true);
    const { data } = await AuthService.updateProfile(req);
    if (data.success) {
      ToastHelper.success({ title: data.message });
      window.localStorage.setItem('user', JSON.stringify(data.user));
      mutate(data.user);
    } else {
      ToastHelper.error({ title: data.message });
    }
    setLoading(false);

    return;
  }
  return (
    <div className="col-span-12  h-full pb-12 md:col-span-10">
      <div className="px-4 pt-4 pb-8">
        <form onSubmit={handleSubmit(submitEditProfile)} className="flex flex-col space-y-8 pb-8 ">
          <div>
            <h3 className="text-2xl font-semibold">Basic Information</h3>
            <hr />
          </div>
          <div className="form-item">
            <InputCustom title="Username" type="text" defaultValue={user.username} className="w-full appearance-none text-black rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200  " disabled={!edit}
              registerField={register('username', {
                required: 'Username required',
              })}
              errors={errors.username}
            />
          </div>
          <div className="form-item">
            <InputCustom
              title="Firstname" type="text" defaultValue={user.firstName} className="w-full appearance-none text-black rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 " disabled={!edit}
              registerField={register('firstName', {
                required: 'First name required',
              })}
              errors={errors.firstName}
            />
          </div>
          <div className="form-item">
            <InputCustom
              title="Lastname" type="text" defaultValue={user.lastName} className="w-full appearance-none text-black rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 " disabled={!edit}
              registerField={register('lastName', {
                required: 'Last name required',
              })}
              errors={errors.lastName}
            />
          </div>
          <div className="form-item">
            <InputCustom
              title="Email" type="text" defaultValue={user.email} className="w-full appearance-none text-black rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 " disabled={!edit}
              registerField={register('email', {
                required: 'Email required',
                pattern: {
                  value: /^\S+@\S+$/i,
                  message: 'Please email pattern (ex: test@mail.com)',
                },
              })}
              errors={errors.email}
            />
          </div>
          {edit && 
          <button
            type="submit"
            className="w-full p-4 py-3 bg-orange-800 rounded-sm
            font-medium text-white uppercase
            focus:outline-none hover:bg-gray-700 hover:shadow-none"
          >      
            {loading ? (
              <FontAwesomeIcon icon={faSpinner} className="spinner" size="sm" />
            ) : (
              'Edit'
            )}
          </button>
        }
        </form>
        
        
      </div>
      
    </div>
  );
}

export default ProfileDetail;